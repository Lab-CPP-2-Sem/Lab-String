#pragma once	//простой вариант для заголовочных файлов

//классический вариант условной компиляции
//#ifndef _String_h_
//#define _String_h_
//обратите внимение на директиву #endif в конце файла

#include <iostream>

/*
Обратите внимание, что большинство методов
реализованы в двух вариантах:
1. с аргументом const String&
2. с аргументом const char*
Второй вариант не обязателен,
т.к. работает неявное приведеление типов.
Компилятор понимает, что должен получить
объект String, но видит аргумент типа char*.
Тогда он начинает искать конструктор класса
String, принимающий char*. Такой конструктор
существует, поэтому создаётся временный объект.
Но в ходе его создания вызывается
потенциально длительная функция strcpy.
Если можно избежать лишнего её вызова,
это следует сделать. Поэтому почти все методы
класса и имеют по 2 версии, с разным типом аргумента.
*/

class String
{
private:
	char* str;
	int len;
	String(const int length);		//приватный(!) конструктор
public:
	String(const char* s = "");		//обратите внимание, что предусмотрено значение по умолчанию
	String(const String& s);		//конструктор копирования
	char& operator[] (const int i);	//доступ к символу по индексу с возможностью редактирования
	const char& operator[] (const int i) const;	//доступ к символу для константных объектов
	String& operator= (const String& s);	
	String& operator= (const char* s);
	String operator+ (const String& s) const;
	String operator+ (const char* s) const;
	String& operator+= (const String& s);
	String& operator+= (const char* s);
	bool isEmpty() const;		//проверка - строка пуста?
	bool operator== (const String& s) const;
	bool operator== (const char* s) const;
	bool operator!= (const String& s) const;
	bool operator!= (const char* s) const;
	bool operator< (const String& s) const;
	bool operator< (const char* s) const;
	bool operator<= (const String& s) const;
	bool operator<= (const char* s) const;
	bool operator> (const String& s) const;
	bool operator> (const char* s) const;
	bool operator>= (const String& s) const;
	bool operator>= (const char* s) const;
	int length() const;		//получение значения приватного поля len
	String& clear();		//очищаем строку, но не удаляем сам объект
	String substring(int begin, int end) const;	//формируем подстроку, копирую символы с индексами от begin до end
	String left(int count) const;	//подстрока длиной count, начиная с левого края
	String right(int count) const;	//подстрока длиной count, начиная с правого края
	bool hasSubstring(const String& s) const;	//проверка на наличие подстроки в строке
	bool hasSubstring(const char* s) const;
	bool hasSymbol(const char symbol) const;	//проверка на наличие символа в строке
	String removeSubstring(int begin, int end) const;	//формируем строку, исключая символы с индексами от begin до end
	String removeSubstring(const String& s) const;		//формируем строку, исключая заданную подстроку
	String removeSubstring(const char* s) const;
	String removeLeft(int count) const;			//формируем строку, исключая count символов слева
	String removeRight(int count) const;		//формируем строку, исключая count символов справа
	String getFirstWord(const String& separator) const;	//формируем строку, в которое записываем первое слово текущей
	String getLastWord(const String& separator) const;	//формируем строку, в которое записываем последнее слово текущей
	~String();

	//перегрузка операций, в случае если первый операнд - строка в стиле Си (char*)
	friend String operator+ (const char* s1, const String& s2);
	friend bool operator== (const char* s1, const String& s2);
	friend bool operator!= (const char* s1, const String& s2);
	friend bool operator> (const char* s1, const String& s2);
	friend bool operator< (const char* s1, const String& s2);
	friend bool operator>= (const char* s1, const String& s2);
	friend bool operator<= (const char* s1, const String& s2);
	
	//перегрузка <<, обеспечивающая вывод с использованием cout
	friend std::ostream& operator<<(std::ostream& os, const String& s);
};

//#endif